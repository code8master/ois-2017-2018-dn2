/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://teaching.lavbic.net/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://teaching.lavbic.net/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://teaching.lavbic.net/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  
  var sporocilo = $('#poslji-sporocilo').val();
  
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }
  preveriOmembo(klepetApp, socket);
  
  aliVideoposnetki(klepetApp, socket);
  $('#poslji-sporocilo').val('');
}


// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('./swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});


/**
 * Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj 
 * z enako dolžino zvezdic (*)
 * 
 * @param vhodni niz
 */
function filtrirajVulgarneBesede(vhod) {
  for (var i in vulgarneBesede) {
    var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
    vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
  }
  return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";


// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    var post = sporocilo.besedilo;
    var vzorec = "https://www.youtube.com/watch?v=";
    post = post.split(' ');
    for (var i=0; i<post.length; i++){
      var aliVeljaven = true;
      for (var j=0; j<vzorec.length; j++){
        if (vzorec.charAt(j)!=post[i].charAt(j)){
          aliVeljaven = false;
          break;
        }
      }
      if (aliVeljaven){
        var x = "";
        for (var y=32; y<post[i].length; y++){
          x += post[i].charAt(y);
        }
        $("#sporocila").append('<iframe src="https://www.youtube.com/embed/' + x + '" style="width:200px; height:150px; margin-left:10px; margin-top:10px;" allowfullscreen></iframe>');
      }
    }
    
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $("#seznam-uporabnikov").empty();
    for (var i=0; i < uporabniki.length; i++) {
      $("#seznam-uporabnikov").append(divElementEnostavniTekst(uporabniki[i]));
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});


/* global $, io, Klepet */


function preveriOmembo(klepetApp, socket){
  var vhodnoBesedilo = $('#poslji-sporocilo').val();
  var sporocilo = "";
  for (var i=0; i<vhodnoBesedilo.length; i++){
    if (vhodnoBesedilo.charAt(i)=='@'){
      var counter = 0;
      var uporabnik = "";
      for (var j=i+1; vhodnoBesedilo.charAt(j)!=' ' && j<vhodnoBesedilo.length; j++){
        uporabnik += vhodnoBesedilo.charAt(j);
        counter++;
      }
      i += counter;
      
      // (\u261E) je enako kot (&#9758;)
      // XSS napad se ne more zgoditi, saj če uporabnik ne obstaja, potem ne pošlje nikomur
      if (uporabnik!=trenutniVzdevek)
        this.socket.emit('sporocilo', {vzdevek: uporabnik, besedilo: "\u261E Omemba v klepetu"});
    }
  }
  
}

function aliVideoposnetki(klepetApp, socket){
  var vzorec = "https://www.youtube.com/watch?v=";
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = sporocilo.split(' ');
  for (var i=0; i<sporocilo.length; i++){
    var aliVeljaven = true;
    for (var j=0; j<vzorec.length; j++){
      if (vzorec.charAt(j)!=sporocilo[i].charAt(j)){
        aliVeljaven = false;
        break;
      }
    }
    if (aliVeljaven){
      var x = "";
      for (var y=32; y<sporocilo[i].length; y++){
        x += sporocilo[i].charAt(y);
      }
      $("#sporocila").append('<iframe src="https://www.youtube.com/embed/' + x + '" style="width:200px; height:150px; margin-left:10px; margin-top:10px;" allowfullscreen></iframe>');
    }
  }
}
